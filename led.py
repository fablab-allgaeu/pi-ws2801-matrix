#!/usr/bin/python
import time
import RPi.GPIO as GPIO
import Adafruit_WS2801
import Adafruit_GPIO.SPI as SPI
 
xmax = 10
ymax = 5
PIXEL_COUNT = ymax*xmax
 
SPI_PORT   = 0
SPI_DEVICE = 0
pixels = Adafruit_WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE), gpio=GPIO)
 
            
pixels.clear() 
pixels.show()


def setMatrixpixel(x,y,r,g,b):
    lednumber = 0
    if y % 2 == 0:
        lednumber = (xmax - x - 1) + (y * xmax)
    else:
        lednumber = x + ( y * xmax)

    print "set x {} y {} pixel {}".format(x,y,lednumber)
    pixels.set_pixel(lednumber, Adafruit_WS2801.RGB_to_color(r, g, b))
    pixels.show()


#from random import randint
#while 1:
#    pixels.clear()
#    for i in range(randint(1,25)):
#        setMatrixpixel(randint(0,xmax-1),randint(0,ymax-1),randint(0,255), randint(0,255), randint(0,255))
#    time.sleep(0.1)

from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello World"

@app.route('/led/<x>/<y>/<r>/<g>/<b>')
def led(x, y, r, g, b):
    setMatrixpixel(int(x), int(y), int(r), int(g), int(b))
    return "ok: {} {} {}/{}/{}".format(x,y,r,g,b)

@app.route('/led/clean')
def clean():
    pixels.clear()
    pixels.show()
    return "ok"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5555)
