#!/bin/sh
set -e
#set -x
set -o pipefail

FILE=$1

OUT=/tmp/bak.ppm

convert $FILE -resize 10x5! -compress none -depth 8 $OUT

Y_COUNTER=0
cat $OUT | grep -v "^#" | tail -n +4 | while read LINE;
do
	#echo $LINE
	X_COUNTER=0
	echo "$LINE " | tr -d "\n" | sed 's,\([[:digit:]]*\) \([[:digit:]]*\) \([[:digit:]]*\) ,\1/\2/\3\n,g' | while read UZZ
	do
	#	echo $UZZ
		curl 192.168.1.147:5555/led/$X_COUNTER/$Y_COUNTER/$UZZ
		echo "$X_COOUNTER - $YCOUNTER"
		X_COUNTER=$(($X_COUNTER+1))
	done
	Y_COUNTER=$(($Y_COUNTER+1))
done

rm $OUT
